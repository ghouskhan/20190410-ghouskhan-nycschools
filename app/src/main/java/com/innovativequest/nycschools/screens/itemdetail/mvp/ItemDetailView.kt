package com.innovativequest.nycschools.screens.itemdetail.mvp
import android.view.View
import com.innovativequest.nycschools.models.ItemDataResponse
import com.innovativequest.nycschools.models.ItemDetailFirst
import io.reactivex.Observable

/**
 * Created by Ghous on 13/04/2019.
 */
interface ItemDetailView {
    val view: View

    fun showError(message: String)

    fun setItem(itemItem: ItemDataResponse, itemDetailFirst: ItemDetailFirst?)

    fun toolbarStartBtnObs(): Observable<Any>
}