package com.innovativequest.nycschools.screens.itemdetail.dagger

import java.lang.annotation.Retention
import java.lang.annotation.RetentionPolicy

import javax.inject.Scope

/**
 * Created by Ghous on 13/04/2019.
 */
@Scope
@Retention(RetentionPolicy.CLASS)
annotation class ItemDetailScope
