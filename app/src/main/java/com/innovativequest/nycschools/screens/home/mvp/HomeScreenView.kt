package com.innovativequest.nycschools.screens.home.mvp
import android.view.View
import com.innovativequest.nycschools.models.ItemDataResponse
import io.reactivex.Observable

/**
 * Created by Ghous on 13/04/2019.
 */
interface HomeScreenView {
    val view: View

    fun showError(message: String)

    fun setLoading(loading: Boolean)

    fun hideKeyBoard()

    fun setListItems(itemList: List<ItemDataResponse>?)

    fun listItemClicks(): Observable<ItemDataResponse>

}