package com.innovativequest.nycschools.screens.home.mvp

import com.innovativequest.nycschools.screens.home.HomeScreenActivity
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import com.innovativequest.nycschools.utils.PreferencesManager
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers


/**
 * Created by Ghous on 13/04/2019.
 */
class HomeScreenPresenter ( private val homeScreenView: HomeScreenView, private  val homeScreenModel: HomeScreenModel ) {

    private val compositeDisposable = CompositeDisposable()

    fun onCreate() {

        compositeDisposable.addAll(
                loadData(),
                subscribeToListItemClicks())
    }

    private fun loadData(): Disposable {
        return homeScreenModel.getItems()
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    try {
                        homeScreenView.setListItems(it)
                    }
                    catch (e: NullPointerException){
                        e.printStackTrace()
                    }
                },
                        { throwable -> homeScreenView.showError(throwable.message!!) })
    }

    private fun subscribeToListItemClicks(): Disposable {
        return homeScreenView.listItemClicks().subscribe {
            homeScreenModel.showItemDetailScreen(it)
        }
    }

    fun onDestroy() {
        compositeDisposable.clear()
    }

}