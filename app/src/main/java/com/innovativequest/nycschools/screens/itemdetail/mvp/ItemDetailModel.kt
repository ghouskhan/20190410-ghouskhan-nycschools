package com.innovativequest.nycschools.screens.itemdetail.mvp

import com.innovativequest.nycschools.models.ItemDataResponse
import com.innovativequest.nycschools.models.ItemDetailSecond
import com.innovativequest.nycschools.models.ItemDetailFirst
import com.innovativequest.nycschools.networkservices.DataService
import com.innovativequest.nycschools.utils.PreferencesManager
import io.reactivex.Observable
import kotlin.collections.ArrayList

/**
 * Created by Ghous on 13/04/2019.
 */
class ItemDetailModel(private val mDataService: DataService) {

    fun getItemDetailFirst() : Observable<ArrayList<ItemDetailFirst>> {
        return mDataService.itemDetailFirst
    }

//    fun getItemDetailSecond() : Observable<ArrayList<ItemDetailSecond>>{
//        return mDataService.itemDetailSecond
//    }

    fun getUpdatedDataItem(dataItem: ItemDataResponse, itemDetailFirstList: List<ItemDetailFirst>) : ItemDetailFirst? {
//        var filteredItem = itemDetailFirstList.first { searchResult -> searchResult.dbn == dataItem.dbn }

        for(itemDetailFirst in itemDetailFirstList){
            if(itemDetailFirst.dbn.equals(dataItem.dbn)){
                return itemDetailFirst
            }
        }
        return null
    }
}


