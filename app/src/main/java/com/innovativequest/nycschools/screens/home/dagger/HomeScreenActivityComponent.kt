package com.innovativequest.nycschools.screens.home.dagger

import com.innovativequest.nycschools.app.builder.RxMvpAppComponent
import com.innovativequest.nycschools.screens.home.HomeScreenActivity
import dagger.Component

/**
 * Created by Ghous on 13/04/2019.
 */
@HomeScreenScope
@Component(modules = arrayOf(HomeScreenModule::class), dependencies = arrayOf(RxMvpAppComponent::class))
interface HomeScreenActivityComponent {
    fun inject(homeScreenActivity: HomeScreenActivity)
}