package com.innovativequest.nycschools.screens.home.mvp

import com.innovativequest.nycschools.models.ItemDataResponse
import com.innovativequest.nycschools.networkservices.DataService
import com.innovativequest.nycschools.screens.home.HomeScreenActivity
import com.innovativequest.nycschools.screens.itemdetail.ItemDetailActivity
import com.innovativequest.nycschools.utils.PreferencesManager
import io.reactivex.Observable
import kotlin.collections.ArrayList

/**
 * Created by Ghous on 13/04/2019.
 */
class HomeScreenModel(private val mDataService: DataService, private val mHomeScreenActivity: HomeScreenActivity,
                      private val mPreferencesManager: PreferencesManager) {

    fun getItems(): Observable<ArrayList<ItemDataResponse>> {
       return mDataService.dataItems
    }

    fun showItemDetailScreen(itemItem: ItemDataResponse){
        ItemDetailActivity.start(mHomeScreenActivity, itemItem)
    }

    fun deleteData(){
        mPreferencesManager.clear()
        //Delete DB data here if required
        // Delete Files and Folder here if required
    }

}


