package com.innovativequest.nycschools.screens.itemdetail.dagger

import com.innovativequest.nycschools.networkservices.DataService
import com.innovativequest.nycschools.screens.itemdetail.ItemDetailActivity
import com.innovativequest.nycschools.screens.itemdetail.mvp.DefaultItemDetailView
import com.innovativequest.nycschools.screens.itemdetail.mvp.ItemDetailModel
import com.innovativequest.nycschools.screens.itemdetail.mvp.ItemDetailPresenter
import com.innovativequest.nycschools.screens.itemdetail.mvp.ItemDetailView
import com.innovativequest.nycschools.utils.PreferencesManager
import com.squareup.picasso.Picasso

import dagger.Module
import dagger.Provides

/**
 * Created by Ghous on 13/04/2019.
 */
@Module
class ItemDetailModule(internal val itemDetailActivity: ItemDetailActivity) {

    @Provides
    @ItemDetailScope
    fun itemDetailView(): ItemDetailView {
        return DefaultItemDetailView(itemDetailActivity)
    }

    @Provides
    @ItemDetailScope
    fun itemDetailPresenter(itemDetailView: ItemDetailView,
                            itemDetailModel: ItemDetailModel,
                            preferencesManager: PreferencesManager): ItemDetailPresenter {
        return ItemDetailPresenter(itemDetailActivity, itemDetailView, itemDetailModel, preferencesManager)
    }

    @Provides
    @ItemDetailScope
    fun itemDetailModel(dataService: DataService): ItemDetailModel {
        return ItemDetailModel(dataService)
    }


}
