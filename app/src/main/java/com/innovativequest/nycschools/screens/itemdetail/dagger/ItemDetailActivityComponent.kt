package com.innovativequest.nycschools.screens.itemdetail.dagger

import com.innovativequest.nycschools.app.builder.RxMvpAppComponent
import com.innovativequest.nycschools.screens.itemdetail.ItemDetailActivity
import dagger.Component

/**
 * Created by Ghous on 13/04/2019.
 */
@ItemDetailScope
@Component(modules = arrayOf(ItemDetailModule::class), dependencies = arrayOf(RxMvpAppComponent::class))
interface ItemDetailActivityComponent {
    fun inject(itemDetailActivity: ItemDetailActivity)
}