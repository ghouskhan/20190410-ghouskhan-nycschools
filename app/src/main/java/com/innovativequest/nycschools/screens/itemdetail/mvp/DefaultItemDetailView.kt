package com.innovativequest.nycschools.screens.itemdetail.mvp

import android.annotation.SuppressLint
import android.support.v7.widget.AppCompatImageView
import android.support.v7.widget.AppCompatTextView
import android.util.Log
import com.innovativequest.nycschools.R
import android.view.View
import android.view.ViewGroup
import android.widget.*
import android.widget.FrameLayout
import android.widget.ImageView
import butterknife.BindView
import butterknife.ButterKnife
import com.innovativequest.nycschools.models.ItemDataResponse
import com.innovativequest.nycschools.models.ItemDetailFirst
import com.innovativequest.nycschools.screens.itemdetail.ItemDetailActivity
import com.jakewharton.rxbinding2.view.RxView
import io.reactivex.Observable

/**
 * Created by Ghous on 13/04/2019.
 */
@SuppressLint("ViewConstructor")
class DefaultItemDetailView (private val itemDetailActivity: ItemDetailActivity) : FrameLayout(itemDetailActivity), ItemDetailView {

    @BindView(R.id.title_bk_actionbar_btn_start)
    internal lateinit var mToolbarStartButton: ImageView

    @BindView(R.id.title_bk_actionbar_btn_end)
    internal lateinit var mToolbarEndButton: ImageView

    @BindView(R.id.item_name_tv)
    internal lateinit var mItemTitle: AppCompatTextView

    @BindView(R.id.sub_item_name_tv)
    internal lateinit var mItemArtist: AppCompatTextView

    @BindView(R.id.body_item_1_tv)
    internal lateinit var mBodyItem1Tv: AppCompatTextView

    @BindView(R.id.body_item_2_tv)
    internal lateinit var mBodyItem2Tv: AppCompatTextView

    @BindView(R.id.body_item_3_tv)
    internal lateinit var mBodyItem3Tv: AppCompatTextView

    @BindView(R.id.body_item_4_tv)
    internal lateinit var mBodyItem4Tv: AppCompatTextView

    @BindView(R.id.body_item_5_tv)
    internal lateinit var mBodyItem5Tv: AppCompatTextView

    @BindView(R.id.body_item_6_tv)
    internal lateinit var mBodyItem6Tv: AppCompatTextView

    @BindView(R.id.item_detail_iv)
    internal lateinit var mItemImageIv: AppCompatImageView

    @BindView(R.id.progress_bar)
    internal lateinit var progressBar: ProgressBar

    override val view: View
        get() = this


    init {
        //Inflate the layout into the viewgroup
        layoutParams = ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
        View.inflate(context, R.layout.default_item_detail, this)
        ButterKnife.bind(this)
        mToolbarEndButton.visibility = GONE
    }

    override fun toolbarStartBtnObs(): Observable<Any> {
        return RxView.clicks(mToolbarStartButton)
    }

    override fun setItem(itemItem: ItemDataResponse, itemDetailFirst: ItemDetailFirst?) {
        itemDetailActivity.runOnUiThread {
            try {
                mItemTitle.text = itemItem.schoolName
                mItemArtist.text = itemItem.location
                mBodyItem1Tv.text = itemDetailActivity.getString(R.string.body_item_1, itemItem.phoneNumber)
                mBodyItem2Tv.text = itemDetailActivity.getString(R.string.body_item_2, itemItem.schoolEmail)

                mBodyItem3Tv.text = itemDetailActivity.getString(R.string.body_item_3, itemDetailFirst?.numOfSatTestTakers)
                mBodyItem4Tv.text = itemDetailActivity.getString(R.string.body_item_4, itemDetailFirst?.satMathAvgScore)
                mBodyItem5Tv.text = itemDetailActivity.getString(R.string.body_item_5, itemDetailFirst?.satWritingAvgScore)
                mBodyItem6Tv.text = itemDetailActivity.getString(R.string.body_item_6, itemDetailFirst?.satCriticalReadingAvgScore)


            }
            catch (e: NullPointerException){
                Log.d(javaClass.name, e.message)
            }
        }
    }

    override fun showError(message: String) {
//        val view = itemDetailActivity.getLayoutInflater().inflate(R.layout.custom_toast_layout, null)
//        Utils.showCustomErrorToast(itemDetailActivity, message, view)
    }

}








