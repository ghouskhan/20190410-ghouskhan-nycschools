package com.innovativequest.nycschools.screens.itemdetail.mvp

import android.util.Log
import com.innovativequest.nycschools.models.ItemDataResponse
import com.innovativequest.nycschools.screens.itemdetail.ItemDetailActivity
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import com.innovativequest.nycschools.utils.PreferencesManager
import io.reactivex.android.schedulers.AndroidSchedulers


/**
 * Created by Ghous on 13/04/2019.
 */
class ItemDetailPresenter (private val activityItem: ItemDetailActivity, private val itemDetailView: ItemDetailView, private  val itemDetailModel: ItemDetailModel,
                           private val preferencesManager: PreferencesManager) {

    private val compositeDisposable = CompositeDisposable()
    private lateinit var mDataItem: ItemDataResponse

    fun onCreate(itemItem: ItemDataResponse) {

        compositeDisposable.addAll(
                subscribeToBackButton(),
                getData())
        mDataItem = itemItem
    }

    private fun subscribeToBackButton(): Disposable {
        return itemDetailView.toolbarStartBtnObs().subscribe {
            activityItem.onBackPressed()
        }
    }

    private fun getData() : Disposable {

        return itemDetailModel.getItemDetailFirst()
                .subscribeOn(AndroidSchedulers.mainThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe( {
                    itemDetailView.setItem(mDataItem, itemDetailModel.getUpdatedDataItem(mDataItem, it))
                },{ Log.d(javaClass.name, it.message)})

        }

    fun onDestroy() {
        compositeDisposable.clear()
    }

}