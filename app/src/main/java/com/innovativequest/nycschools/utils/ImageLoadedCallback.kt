package com.innovativequest.nycschools.utils

import android.widget.ProgressBar
import com.squareup.picasso.Callback

open class ImageLoadedCallback(internal var progressBar: ProgressBar?) : Callback {

    override fun onSuccess() {

    }

    override fun onError() {

    }
}