package com.innovativequest.nycschools.utils;

/**
 * Created by Ghous on 13/04/2019.
 */
public class AppConstants {

    private static final String TAG = "AppConstants";


    // ENV DEPENDENT BASE URLs
    public static final String BASE_URL_DEV = "https://data.cityofnewyork.us/";
    private static final String DEFAULT_LOCALE = "en-GB";

    public static String getLocale()
    {
        return DEFAULT_LOCALE;
    }

    public static final String GET_POSTS = "resource/s3k6-pzi2.json";
    public static final String GET_USERS = "resource/f9bf-2cp4.json";
    public static final String GET_COMMENTS = "comments";
}

