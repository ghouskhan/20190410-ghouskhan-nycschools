package com.innovativequest.nycschools.networkservices;

import com.innovativequest.nycschools.models.ItemDataResponse;
import com.innovativequest.nycschools.models.ItemDetailSecond;
import com.innovativequest.nycschools.models.ItemDetailFirst;
import com.innovativequest.nycschools.utils.AppConstants;

import java.util.ArrayList;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by Ghous on 13/04/2019.
 */


public interface DataService {
    //************************************************
    //HEAD REQUESTS
    //************************************************

    //************************************************
    // GET REQUESTS
    //************************************************
    @GET(AppConstants.GET_POSTS)
    Observable<ArrayList<ItemDataResponse>> getDataItems();

    @GET(AppConstants.GET_USERS)
    Observable<ArrayList<ItemDetailFirst>> getItemDetailFirst();

    @GET(AppConstants.GET_COMMENTS)
    Observable<ArrayList<ItemDetailSecond>> getItemDetailSecond();

    //************************************************
    // POST REQUESTS
    //************************************************


}